#include<stdio.h>
#include<sys/wait.h>
#include <fcntl.h>
#include<unistd.h>
#include<string.h>

int main()
{
    int ret , i ,arr1[2],arr2[2],pid,num1,num2,result;
    ret = pipe(arr1);
    if(ret<0)
    {
        printf("pipe() failed.\n");
        _exit(1);
    }
    ret = pipe(arr2);
    if(ret<0)
    {
        printf("pipe() failed.\n");
        _exit(1);
    }
    pid = fork();
    if(pid==0)
    {
        close(arr1[0]);
        close(arr2[1]);
        printf("Enter num1  :   ");
        scanf("%d",&num1);
        write(arr1[1],&num1,4);
        printf("Enter num2  :   ");
        scanf("%d",&num2);
        write(arr1[1],&num2,4);
        read(arr2[0],&result,4);
        printf("Result  :   %d\n",result);
        close(arr1[1]);
        close(arr2[0]);
    }
    else
    {
        close(arr2[0]);
        close(arr1[1]);
        read(arr1[0],&num1,4);
        read(arr1[0],&num2,4);
        result = num1 + num2;
        write(arr2[1],&result,4);
        close(arr2[1]);
        close(arr1[0]);

    }
}