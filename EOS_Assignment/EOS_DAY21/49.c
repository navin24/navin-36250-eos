#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
    int pid1, pid2, ret, err, s, arr[2];
    printf("parent started!\n");

    ret = pipe(arr);
    if(ret < 0) {
        perror("pipe() failed");
        _exit(1);
    }

    pid1 = fork();
    if(pid1 == 0) {
        close(arr[0]);
        dup2(arr[1], STDOUT_FILENO);
        close(arr[1]);

        err = execlp("ls", "ls", "-l", NULL);
        if(err < 0) {
            perror("exec() failed");
            _exit(1);
Y        }
    } 

    pid2 = fork();
    if(pid2 == 0) {
        close(arr[1]);
        dup2(arr[0], STDIN_FILENO);
        close(arr[0]);

        err = execlp("wc", "wc", NULL);
        if(err < 0) {
            perror("exec() failed");
            _exit(1);
        }
    }

    close(arr[1]);
    close(arr[0]);

    waitpid(pid1, &s, 0);
    waitpid(pid2, &s, 0);

    printf("parent finished!\n");
    return 0;
}