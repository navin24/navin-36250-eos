#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

// terminal> mkfifo /tmp/desd_fifo 

#define FIFO_PATH "/tmp/desd_fifo"

int main() {
    int fd;
    char buf[40];

    fd = open(FIFO_PATH, O_WRONLY);
    if(fd < 0) {
        perror("failed to open() fifo");
        _exit(1);
    }

    printf("writer: enter a string: ");
    scanf("%s", buf);

    write(fd, buf, strlen(buf)+1);
    printf("writer: sent = %s\n", buf);

    close(fd);
    return 0;
}
