#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SOCK_PATH "/tmp/desd_sock"

int main() {
    int ret, client_fd;
    struct sockaddr_un server_addr;
    char buf[512];
    //4. create client socket
    client_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    printf("client socket created: %d\n", client_fd);

    //5. connect to the server socket
    printf("client is connecting to server socket.\n");
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, SOCK_PATH);
    ret = connect(client_fd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    printf("client is connected to server socket: %d\n", ret);

    do {
        //7. write to the socket
        printf("server: ");
        fgets(buf, sizeof(buf), stdin);
        write(client_fd, buf, strlen(buf)+1);

        //10. read from the socket
        read(client_fd, buf, sizeof(buf));
        printf("client: %s", buf);
    }while(strcmp(buf, "bye\n") != 0);

    //11. close the socket
    close(client_fd);
    printf("client closed socket connection.\n");
    return 0;
}
