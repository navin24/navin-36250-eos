#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<string.h>
#include<sys/un.h>
#define SOCK_PATH "/home/sunbeam/Assignments/EOS_Assignment/EOS_DAY21/desd_sock1"

int main()
{
    int server_fd,client_fd,ret;
    struct sockaddr_un ser_addr,client_addr;
    server_fd = socket(AF_UNIX,SOCK_STREAM,0);
    if(server_fd<0)
    {
        perror("Server connection has not established.\n");
        _exit(2);
    }
    printf("Server Connection has been established.\n");

    ser_addr.sun_family = AF_UNIX;
    strcpy(ser_addr.sun_path,SOCK_PATH);
    ret = bind(server_fd,(struct sockaddr*)&ser_addr,sizeof(ser_addr));
    if(ret<0)
    {
        perror("bind() failed.\n");
        _exit(3);
    }
    printf("%d\n",ret);

    return 0;
}