#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<sys/socket.h>
#include <sys/un.h>
int main()
{
    int client_fd;
    client_fd = socket(AF_UNIX,SOCK_STREAM,0);
    if(client_fd<0)
    {
        perror("Client connection has not established.\n");
        _exit(1);
    }
    printf("Client Connection has been established.\n");
    return 0;
}