#include <stdio.h>
#include <unistd.h>
#include<string.h>
#include<signal.h>

int count =0;
void sigpipe_handler(int num)
{
    printf("Count   :   %d\n",count);
    _exit(1);
}
int main()
{
    int ret,s,i;
    int arr[2];
    struct sigaction sa;
    char a[1]="a";
    
    memset(&sa,0,sizeof(sa));
    sa.sa_handler = sigpipe_handler;
    ret = sigaction(SIGPIPE,&sa, NULL);
    if(ret!=0)
    {
        perror("Sigaction() failed.\n");
        _exit(2);
    }
    ret = pipe(arr);
    if(ret<0)
    {
        printf("pipe() failed\n");
        _exit(1);
    }
      while(1)
      {
             ++count;
             ret = write(arr[1],a,sizeof(a));
      }
    return 0;
}

