#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#define FIFO_PATH "/tmp/desd_fifo"

int main() {
    int fd;
    char buf[40];

    fd = open(FIFO_PATH, O_RDONLY);
    if(fd < 0) {
        perror("failed to open() fifo");
        _exit(1);
    }

    printf("reader: waiting for writer message.\n");
    read(fd, buf, sizeof(buf));
    printf("reader: received = %s\n", buf);

    close(fd);
    return 0;
}
