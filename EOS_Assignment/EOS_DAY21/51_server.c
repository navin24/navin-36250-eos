#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SOCK_PATH "/tmp/desd_sock"

int main() {
    int ret, server_fd, client_fd;
    struct sockaddr_un server_addr, client_addr;
    socklen_t sock_len;
    char buf[512];

    //1. create main server socket
    server_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    printf("server socket created: %d\n", server_fd);

    //2. assign/bind address to the socket
    server_addr.sun_family = AF_UNIX;
    strcpy(server_addr.sun_path, SOCK_PATH);
    ret = bind(server_fd, (struct sockaddr*)&server_addr, sizeof(server_addr));
    printf("server address assigned: %d\n", ret);

    //3. listen to the socket
    listen(server_fd, 5);
    printf("listening to server socket.\n");

    //6. accept the client connection & create new socket
    printf("server is waiting for client connection.\n");
    sock_len = sizeof(client_addr);
    client_fd = accept(server_fd, (struct sockaddr*)&client_addr, &sock_len);
    printf("server accepted client connection: %d\n", client_fd);

    do {
        //8. read from the socket
        read(client_fd, buf, sizeof(buf));
        printf("client: %s", buf);

        //9. write from the socket
        printf("server: ");
        fgets(buf, sizeof(buf), stdin);
        write(client_fd, buf, strlen(buf)+1);
    }while(strcmp(buf, "bye\n") != 0);

    //12. close the socket
    close(client_fd);
    printf("server closed client socket connection.\n");

    //13. shutdown the main socket
    shutdown(server_fd, SHUT_RDWR);
    printf("server closed main socket socket.\n");
    return 0;
}
