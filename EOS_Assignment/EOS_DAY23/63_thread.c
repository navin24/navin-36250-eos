#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/syscall.h>

int gettid() {
    return syscall(SYS_gettid);
}

void* my_thread_fn(void *param) {
    int i, ret;
    // block the SIGINT signal
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    ret = pthread_sigmask(SIG_SETMASK, &set, NULL);
    if(ret < 0)
        perror("pthread_sigmask() failed");
    
    for(i=1; i<=30; i++) {
        printf("thread (%d): %d\n", gettid(), i);
        sleep(1);
    }
    return NULL;
} 

// signal handler is executed by the thread, to which signal is sent.
void sigint_handler(int sig) {
    printf("signal received in thread %d in process %d: %d\n", 
        gettid(), getpid(), sig);    
}

int main() {
    pthread_t th;
    int ret;
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT, &sa, NULL);

    ret = pthread_create(&th, NULL, my_thread_fn, NULL);
    printf("press enter to send signal to the thread ...\n");
    getchar();
    ret = pthread_kill(th, SIGINT); // signal sent to the thread
    printf("signal is sent to the thread.\npress enter to exit...\n");
    getchar();
    printf("bye!\n");
    return 0;
}
