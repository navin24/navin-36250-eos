#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <sys/syscall.h>

int gettid() {
    return syscall(SYS_gettid);
}

void* my_thread_fn(void *param) {
    int i;
    for(i=1; i<=20; i++) {
        printf("thread(%d)%d: %d\n",getpid(),gettid(), i);
        sleep(1);
    }
    return NULL;
} 

void sigint_handler(int sig) {
    printf("signal received in thread %d in process %d: %d\n", gettid(), getpid(),sig);    
}

int main() {
    pthread_t th;
    int ret;
    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT, &sa, NULL);

    ret = pthread_create(&th, NULL, my_thread_fn, NULL);
    printf("press enter to send signal to the thread ...\n");
    getchar();
    ret = pthread_kill(th, SIGINT); 
    printf("signal is sent to the thread.\npress enter to exit...\n");
    getchar();
    printf("bye!\n");
    return 0;
}

