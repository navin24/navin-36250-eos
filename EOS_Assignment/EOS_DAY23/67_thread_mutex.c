#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/syscall.h>

// global variable
int cnt = 0;
pthread_mutex_t m;

int gettid() {
    return syscall(SYS_gettid);
}

void* incr_fn(void *param) {
    int i;
    for(i=1; i<=10; i++) {
        // lock(m)
        pthread_mutex_lock(&m);
        cnt++;
        printf("incr: %d\n", cnt);
        // unlock(m)
        pthread_mutex_unlock(&m);
        sleep(1);
    }
    return NULL;
} 

void* decr_fn(void *param) {
    int i;
    for(i=1; i<=10; i++) {
        // lock(m)
        pthread_mutex_lock(&m);
        cnt--;
        printf("decr: %d\n", cnt);
        // unlock(m)
        pthread_mutex_unlock(&m);
        sleep(1);
    }
    return NULL;
} 

int main() {
    pthread_t it, dt;
    int ret;
    // initialize the mutex (with default mutex attributes)
    pthread_mutex_init(&m, NULL); 
    ret = pthread_create(&it, NULL, incr_fn, NULL);
    ret = pthread_create(&dt, NULL, decr_fn, NULL);
    printf("main is waiting for completing both the thread.\n");
    pthread_join(it, NULL);
    pthread_join(dt, NULL);
    printf("final count: %d\n", cnt);
    pthread_mutex_destroy(&m);
    return 0;
}
