#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <semaphore.h>

#define SHM_KEY 0x2809
#define SEM_KEY 0x1506

typedef struct shm {
    int cnt;
    sem_t s;
}shm_t;

int main() {
    int ret, i, shmid, s;
    shm_t *ptr;

    // create shared memory object
    shmid = shmget(SHM_KEY, sizeof(shm_t), IPC_CREAT | 0600);
    if(shmid < 0) {
        perror("shmget() failed");
        _exit(1);
    }

    // attach shm region to the process & get its pointer
    ptr = (shm_t*)shmat(shmid, NULL, 0);
    if(ptr == (void*)-1) {
        perror("shmat() failed");
        shmctl(shmid, IPC_RMID, NULL);
        _exit(2);
    }
    ptr->cnt = 0;
    // initialize semaphore count to 1.
    // sem_init() arg2 pshared - non-zero - it can accessed across the process.
    // semaphore must be declared within shared memory.
    sem_init(&ptr->s, SEM_KEY, 1);

    shmctl(shmid, IPC_RMID, NULL);

    ret = fork();
    if(ret == 0) {
        // child process
        for(i=1; i<=10; i++) {
            // P(s)
            sem_wait(&ptr->s);
            ptr->cnt--;
            printf("child: %d\n", ptr->cnt);
            // V(s)
            sem_post(&ptr->s);
            sleep(1);
        }
        shmdt(ptr); // nattach = 1
    } else {
        // parent process
        for(i=1; i<=10; i++) {
            // P(s)
            sem_wait(&ptr->s);
            ptr->cnt++;
            printf("parent: %d\n", ptr->cnt);
            // V(s)
            sem_post(&ptr->s);
            sleep(1);
        }
        waitpid(-1, &s, 0);
        printf("final count: %d\n", ptr->cnt);
        sem_destroy(&ptr->s);
        shmdt(ptr); // nattach = 0
        printf("shmdt() is done from parent.\n");
        printf("bye!\n");
    }
    return 0;
}