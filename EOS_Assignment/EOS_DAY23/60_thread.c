#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void* my_thread_fn(void *param) {
    int i;
    for(i=1; i<=20; i++) {
        printf("thread: %d\n", i);
        sleep(1);
    }
    return NULL;
} 

int main() {
    pthread_t th;
    int ret;
    ret = pthread_create(&th, NULL, my_thread_fn, NULL);
    printf("press enter to cancel the thread ...\n");
    getchar();
    ret = pthread_cancel(th); //it terminates the thread by pthread_cancel while pthread_exit completes the function first.
    printf("thread is cancelled.\npress enter to exit...\n");
    getchar();
    printf("bye!\n");
    return 0;
}
