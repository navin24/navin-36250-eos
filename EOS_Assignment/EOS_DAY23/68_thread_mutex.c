#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <pthread.h>

#define SHM_KEY 0x2809

typedef struct shm {
    int cnt;
    pthread_mutex_t m;
}shm_t;

int main() {
    int ret, i, shmid, s;
    pthread_mutexattr_t ma;
    shm_t *ptr;

    // create shared memory object
    shmid = shmget(SHM_KEY, sizeof(shm_t), IPC_CREAT | 0600);
    if(shmid < 0) {
        perror("shmget() failed");
        _exit(1);
    }

    // attach shm region to the process & get its pointer
    ptr = (shm_t*)shmat(shmid, NULL, 0);
    if(ptr == (void*)-1) {
        perror("shmat() failed");
        shmctl(shmid, IPC_RMID, NULL);
        _exit(2);
    }
    ptr->cnt = 0;
    // initialize mutex so that it can be accessed across the processes.
    // set its pshared attribute to PTHREAD_PROCESS_SHARED.
    pthread_mutexattr_init(&ma);
    pthread_mutexattr_setpshared(&ma, PTHREAD_PROCESS_SHARED);
    pthread_mutex_init(&ptr->m, &ma);

    shmctl(shmid, IPC_RMID, NULL);

    ret = fork();
    if(ret == 0) {
        // child process
        for(i=1; i<=10; i++) {
            // lock(m)
            pthread_mutex_lock(&ptr->m);
            ptr->cnt--;
            printf("child: %d\n", ptr->cnt);
            // unlock(m)
            pthread_mutex_unlock(&ptr->m);
            sleep(1);
        }
        shmdt(ptr); // nattach = 1
    } else {
        // parent process
        for(i=1; i<=10; i++) {
            // lock(m)
            pthread_mutex_lock(&ptr->m);
            ptr->cnt++;
            printf("parent: %d\n", ptr->cnt);
            // lock(m)
            pthread_mutex_unlock(&ptr->m);
            sleep(1);
        }
        waitpid(-1, &s, 0);
        printf("final count: %d\n", ptr->cnt);
        pthread_mutex_destroy(&ptr->m);
        shmdt(ptr); // nattach = 0
        printf("shmdt() is done from parent.\n");
        printf("bye!\n");
    }
    return 0;
}
