#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/syscall.h>
#include <semaphore.h>

// global variable
int cnt = 0;
sem_t s;

int gettid() {
    return syscall(SYS_gettid);
}

void* incr_fn(void *param) {
    int i;
    for(i=1; i<=10; i++) {
        // P(s)
        sem_wait(&s);
        cnt++;
        printf("incr: %d\n", cnt);
        // V(s)
        sem_post(&s);
        sleep(1);
    }
    return NULL;
} 

void* decr_fn(void *param) {
    int i;
    for(i=1; i<=10; i++) {
        // P(s)
        sem_wait(&s);
        cnt--;
        printf("decr: %d\n", cnt);
        // V(s)
        sem_post(&s);
        sleep(1);
    }
    return NULL;
} 

int main() {
    pthread_t it, dt;
    int ret;
    // initial: s=1
    sem_init(&s, 0, 1);
    ret = pthread_create(&it, NULL, incr_fn, NULL);
    ret = pthread_create(&dt, NULL, decr_fn, NULL);
    printf("main is waiting for completing both the thread.\n");
    pthread_join(it, NULL);
    pthread_join(dt, NULL);
    printf("final count: %d\n", cnt);
    sem_destroy(&s);
    return 0;
}

