#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/syscall.h>

// global variable
int cnt = 0;

int gettid() {
    return syscall(SYS_gettid);
}

void* incr_fn(void *param) {
    int i;
    for(i=1; i<=10; i++) {
        cnt++;
        printf("incr: %d\n", cnt);
        sleep(1);
    }
    return NULL;
} 

void* decr_fn(void *param) {
    int i;
    for(i=1; i<=10; i++) {
        cnt--;
        printf("decr: %d\n", cnt);
        sleep(1);
    }
    return NULL;
} 

int main() {
    pthread_t it, dt;
    int ret;
    ret = pthread_create(&it, NULL, incr_fn, NULL);
    ret = pthread_create(&dt, NULL, decr_fn, NULL);
    printf("main is waiting for completing both the thread.\n");
    pthread_join(it, NULL);
    pthread_join(dt, NULL);
    printf("final count: %d\n", cnt);
    return 0;
}
