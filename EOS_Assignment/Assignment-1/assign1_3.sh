#!/bin/bash

echo -e "\nProgram for whether year is leap year or not"

echo  "Enter Year:"
read year

if [ `expr $year % 4` -eq 0 -a `expr $year % 100` -ne 0 -o `expr $year % 400` -eq 0 ]
then
  	echo "Year is leap Year"
else
  	echo "Year is not leap Year"
fi

