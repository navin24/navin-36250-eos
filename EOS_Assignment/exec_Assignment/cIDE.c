#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/wait.h>

int main(void)
{
    char fname[30],oname[20],exec_name[20];
    int ch,ret,pid,status;
    do
    {
        printf("0.Exit\n1.Compile\n2.Link\n3.Run\n");
        printf("Enter choice:");
        scanf("%d",&ch);
        switch(ch)
        {
            case 0:
                break;
            case 1:
                printf("Compile\n");
                printf("Enter .c file for compilation:\n");
                scanf("%s",fname);
                ret=fork();
                if(ret==0)
                {
                    char *args[]={"gcc","-c",fname,NULL};
                    pid=execv("/usr/bin/gcc",args);
                if(pid<0)
                {
                    perror("exec() failed");
                    _exit(1);
                }
                }
                else
                {
                    waitpid(-1,&status,0);
                    printf("Child exit status: %d\n",WEXITSTATUS(status));
                }                
                break;
            case 2:
                printf("Link\n");
                printf("Enter .o file for linking with out file:\n");
                scanf("%s",oname);
                printf("Enter .out file:\n");
                scanf("%s",exec_name);
                ret=fork();
                if(ret==0)
                {
                    char *args[]={"gcc","-o",exec_name,oname,NULL};
                    pid=execv("/usr/bin/gcc",args);
                if(pid<0)
                {
                    perror("exec() failed");
                    _exit(2);
                }
                }
                else
                {
                    waitpid(-1,&status,0);
                    printf("Child exit status: %d\n",WEXITSTATUS(status));
                }                
                
                break;
            case 3:
                printf("Run\n");
                ret=fork();
                if(ret==0)
                {
                    char *args[]={exec_name,NULL};
                    pid=execv(exec_name,args);
                if(pid<0)
                {
                    perror("exec() failed");
                    _exit(3);
                }
                }
                else
                {
                    waitpid(-1,&status,0);
                    printf("Child exit status: %d\n",WEXITSTATUS(status));
                }
                break; 
            default:
                printf("Invalid Choice\n");
                break;
       }
    }while(ch!=0);
}