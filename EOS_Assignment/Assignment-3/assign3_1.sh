#!/bin/bash

echo -n "Enter basic salary:"
read bs

hra=`echo "$bs * 0.2" | bc`
da=`echo "$bs * 0.4" | bc`

gross_salary=`echo "$bs + $hra + $da" | bc`

echo "Gross Salary : $gross_salary"
