#!/bin/bash

echo Enter first filename
read first
echo Enter second filename
read second
cat $first | tr "a-z|A-Z" "A-Z|a-z" >> $second
echo "Data in first file"
cat $first

echo "Data after concatenation and reverse case in second file"
cat $second
