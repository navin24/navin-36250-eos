#!/bin/bash

echo "Executables files are:"

echo "Method 1"
ls -F | grep '*$'
echo ""
echo "Method 2"
find . -type f -executable -print
