#!/bin/bash

echo -n "Enter number of elements in an array:"
read num

echo "Enter array elemnts"
i=0
while [ $i -lt $num ]
do
	echo -n "Element $i:"
	read arr[$i]
	i=`expr $i + 1`
done
max=${arr[0]}
j=1
while [ $j -lt $num ]
do 
	if [ ${arr[j]} -gt $max ]
	then 
		max=${arr[j]}
	fi
		j=`expr $j + 1`
done
echo "Maximum Number : $max"
min=${arr[0]}
j=1
while [ $j -lt $num ]
do 
	if [ ${arr[j]} -lt $min ]
	then 
		min=${arr[j]}
	fi
		j=`expr $j + 1`
done
echo "Minimum Number : $min"

