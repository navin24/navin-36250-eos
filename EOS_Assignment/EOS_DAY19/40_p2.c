#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <signal.h>

#define SHM_KEY 0x2809
#define SEM_KEY 0x1506

typedef struct shm {
    int cnt;
    // ...
}shm_t;

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO (Linux-specific) */
};

int main() {
    int ret, i, s;
    int shmid, semid;
    shm_t *ptr;
    union semun su;
    struct sembuf ops[1];

    // get semaphore created by p1 process.
    semid =  semget(SEM_KEY, 1, 0);
    if(semid < 0) {
        perror("semget() failed");
        fprintf(stderr, "execute p1 process first.\n");
        _exit(1);
    }

    // get shared memory created by p1 process.
    shmid = shmget(SHM_KEY, sizeof(shm_t), 0);
    if(shmid < 0) {
        perror("shmget() failed");
        _exit(1);
    }

    // attach shm region to the process & get its pointer
    ptr = (shm_t*)shmat(shmid, NULL, 0);
    if(ptr == (void*)-1) {
        perror("shmat() failed");
        _exit(2);
    }

    for(i=1; i<=20; i++) {
        // P(s);
        ops[0].sem_num = 0;
        ops[0].sem_op = -1;
        ops[0].sem_flg = 0;
        semop(semid, ops, 1);

        ptr->cnt--;
        printf("p2: %d\n", ptr->cnt);
        sleep(1);

        // V(s);
        ops[0].sem_num = 0;
        ops[0].sem_op = +1;
        ops[0].sem_flg = 0;
        semop(semid, ops, 1);
    }
    shmdt(ptr);

    printf("bye!\n");
    return 0;
}