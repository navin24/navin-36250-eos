#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/sem.h>

#define MAX 5
typedef struct circular_queue
{
    int arr[MAX];
    int front, rear;
    int count;
}cirque_t;

void cq_init(cirque_t *this) {
    memset(this->arr, 0, sizeof(this->arr));
    this->count = 0;
    this->front = -1;
    this->rear = -1;
}

void cq_push(cirque_t *this, int val) {
    this->rear = (this->rear + 1) % MAX;
    this->arr[this->rear] = val;
    this->count++;
}

int cq_pop(cirque_t *this) {
    this->front = (this->front + 1) % MAX;
    this->count--;
    return this->arr[this->front];
}

int cq_peek(cirque_t *this) {
    int index = (this->front + 1) % MAX;
    return this->arr[index];
}

int cq_empty(cirque_t *this) {
    return this->count == 0;
}

int cq_full(cirque_t *this) {
    return this->count == MAX;
}


#define SHM_KEY 0x2809
#define SEM_KEY 0x1506

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO (Linux-specific) */
};

enum sem_cntr {
    S_MUTEX=0, S_FILLED=1, S_EMPTY=2
};

// global variables
int semid;

// implement sigint signal handler
void sigint_handler(int sig) {
    // destroy semaphore
    semctl(semid, 0, IPC_RMID);
    printf("bye!\n");
    exit(0);
}


int main() {
    int shmid, ret;
    cirque_t *q;
    int val;
    struct sigaction sa;
    unsigned short sem_init_cnt[3];
    union semun su;
    struct sembuf ops[2];

    // register signal handler
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT, &sa, NULL);

    // create semaphore with three counters (mutex, filled, empty)
    semid = semget(SEM_KEY, 3, IPC_CREAT | 0600);

    // initialize semaphore counters (mutex=1, filled=0, empty=MAX)
    sem_init_cnt[S_MUTEX] = 1;
    sem_init_cnt[S_FILLED] = 0;
    sem_init_cnt[S_EMPTY] = MAX; 
    su.array = sem_init_cnt;
    ret = semctl(semid, 0, SETALL, su);

    // create shared memory to share circular queue
    shmid = shmget(SHM_KEY, sizeof(cirque_t), IPC_CREAT | 0600);

    // get the pointer to the shared memory
    q = (cirque_t*) shmat(shmid, NULL, 0);

    // initialize circular queue
    cq_init(q);

    // mark shared memory for destruction
    ret = shmctl(shmid, IPC_RMID, NULL);

    // create new process
    ret = fork();
    if(ret == 0) {
        // child process (producer)
        while(1) {
            // P(empty, mutex)
            ops[0].sem_num = S_EMPTY;
            ops[0].sem_op = -1;
            ops[0].sem_flg = 0;
            ops[1].sem_num = S_MUTEX;
            ops[1].sem_op = -1;
            ops[1].sem_flg = 0;
            semop(semid, ops, 2);

            // add random data into circular queue
            val = rand() % 100;
            cq_push(q, val);
            printf("WR : %d\n", val);
            sleep(1);

            // V(mutex, filled)
            ops[0].sem_num = S_FILLED;
            ops[0].sem_op = +1;
            ops[0].sem_flg = 0;
            ops[1].sem_num = S_MUTEX;
            ops[1].sem_op = +1;
            ops[1].sem_flg = 0;
            semop(semid, ops, 2);

            sleep(10);
        }
    }
    else {
        // parent process (consumer)
        while(1) {
            // P(filled, mutex)
            ops[0].sem_num = S_FILLED;
            ops[0].sem_op = -1;
            ops[0].sem_flg = 0;
            ops[1].sem_num = S_MUTEX;
            ops[1].sem_op = -1;
            ops[1].sem_flg = 0;
            semop(semid, ops, 2);

            // remove from circular queue and print it
            val = cq_pop(q);
            printf("RD: %d\n", val);
            sleep(1);

            // V(mutex, empty)
            ops[0].sem_num = S_EMPTY;
            ops[0].sem_op = +1;
            ops[0].sem_flg = 0;
            ops[1].sem_num = S_MUTEX;
            ops[1].sem_op = +1;
            ops[1].sem_flg = 0;
            semop(semid, ops, 2);
        }
    }
    return 0;
}