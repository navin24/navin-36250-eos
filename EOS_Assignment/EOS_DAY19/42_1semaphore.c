#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/sem.h>

#define SEM_KEY 0x2405

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO (Linux-specific) */
};

int main() {
    int ret, i, s;
    const char *str1 = "SUNBEAM\n";
    const char *str2 = "INFOTECH\n";
    int semid;
    union semun su;
    struct sembuf ops[1];

    // create semaphore
    semid = semget(SEM_KEY, 1, IPC_CREAT | 0600);
    if(semid < 0) {
        perror("semget() failed");
        _exit(1);
    }

    // init semaphore
    su.val = 0;
    ret = semctl(semid, 0, SETVAL, su);
    if(ret < 0) {
        perror("semctl() failed");
        _exit(1);
    }
    ret = fork();
    if(ret == 0) {
        // child process
        for(i=0; str1[i]!='\0'; i++) {
            putchar(str1[i]);
            fflush(stdout);
            sleep(1);
        }

        // V(s);
        ops[0].sem_num = 0;
        ops[0].sem_op = +1;
        ops[0].sem_flg = 0;
        semop(semid, ops, 1);
    } else {
        // parent process
        // P(s);
        ops[0].sem_num = 0;
        ops[0].sem_op = -1;
        ops[0].sem_flg = 0;
        semop(semid, ops, 1);

        for(i=0; str2[i]!='\0'; i++) {
            putchar(str2[i]);
            fflush(stdout);
            sleep(1);
        }

        waitpid(-1, &s, 0);
		
        semctl(semid, 0, IPC_RMID);
	}
    return 0;
}

