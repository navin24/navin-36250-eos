#include <stdio.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <signal.h>
#include <string.h>

#define SHM_KEY 0x2809
#define SEM_KEY 0x1506

typedef struct shm {
    int cnt;
    // ...
}shm_t;

union semun {
    int              val;    /* Value for SETVAL */
    struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
    struct seminfo  *__buf;  /* Buffer for IPC_INFO (Linux-specific) */
};

// global variables
int shmid, semid;

void sigint_handler(int sig) {
    shmctl(shmid, IPC_RMID, NULL);
    semctl(semid, 0, IPC_RMID);
    printf("bye!\n");
    _exit(0);
}

int main() {
    int ret, i, s;
    shm_t *ptr;
    union semun su;
    struct sembuf ops[1];
    struct sigaction sa;

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT, &sa, NULL);
    if(ret < 0) {
        perror("sigaction() failed");
        _exit(1);
    }

    // create semaphore with single counter
    semid =  semget(SEM_KEY, 1, IPC_CREAT | 0600);
    if(semid < 0) {
        perror("semget() failed");
        _exit(1);
    }

    // initialize semaphore counter = 1 (mutual exclusion use-case)
    su.val = 1;
    ret = semctl(semid, 0, SETVAL, su);
    if(ret < 0) {
        perror("semctl() failed");
        _exit(1);
    }

    // create shared memory object
    shmid = shmget(SHM_KEY, sizeof(shm_t), IPC_CREAT | 0600);
    if(shmid < 0) {
        perror("shmget() failed");
        _exit(1);
    }

    // attach shm region to the process & get its pointer
    ptr = (shm_t*)shmat(shmid, NULL, 0);
    if(ptr == (void*)-1) {
        perror("shmat() failed");
        shmctl(shmid, IPC_RMID, NULL);
        _exit(2);
    }
    ptr->cnt = 0;

    for(i=1; i<=20; i++) {
        // P(s);
        ops[0].sem_num = 0;
        ops[0].sem_op = -1;
        ops[0].sem_flg = 0;
        semop(semid, ops, 1);

        ptr->cnt++;
        printf("p1: %d\n", ptr->cnt);
        sleep(1);

        // V(s);
        ops[0].sem_num = 0;
        ops[0].sem_op = +1;
        ops[0].sem_flg = 0;
        semop(semid, ops, 1);
    }
    shmdt(ptr);

    printf("press ctrl+c to exit ...\n");
    pause();
    return 0;
}