#include<stdio.h>
#include<unistd.h>
#include<sys/syscall.h>
#include<pthread.h>

int gettid()
{
    int tid = syscall(SYS_gettid);
    return tid;
}
void* print_table(void* param)
{
    int i,num=(long)param;
    for(i=1;i<=10;i++)
    {
        printf("%ld * %d = %ld\n",num,i,num * i);
        sleep(1);
    }
    return NULL;
}
int main()
{
    int i,ret;
    pthread_t t1,t2,t3;
    printf("program started(%d).\n",getpid());
    ret = pthread_create(&t1,NULL,print_table,(void*)5L);
    ret = pthread_create(&t1,NULL,print_table,(void*)4L);
    ret = pthread_create(&t1,NULL,print_table,(void*)2L);
    getchar();
    printf("program completed.\n");
    return 0;
}