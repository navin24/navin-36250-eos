#include<stdio.h>
#include<unistd.h>
#include<sys/syscall.h>
#include<pthread.h>

int gettid()
{
    int tid = syscall(SYS_gettid);
    return tid;
}
void* arr_sum(void* param)
{
    int* arr = (int*)param;
    long total= 0;
    int i;
    for(i=0;i<5;i++)
        total = total + arr[i];
    
    return (void*)total;
}
int main()
{
    int i,ret;
    int arr[] = {1,2,3,4,5};
    pthread_t t1;
    long result;
    printf("program started(%d).\n",getpid());
    pthread_create(&t1,NULL,arr_sum,arr);
    pthread_join(t1,(void**)&result);
    printf("Result  :   %ld\n",result);
    printf("program completed.\n");
    return 0;
}