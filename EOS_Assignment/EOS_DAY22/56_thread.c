#include<stdio.h>
#include<unistd.h>
#include<sys/syscall.h>
#include<pthread.h>

int gettid()
{
    int tid = syscall(SYS_gettid);
    return tid;
}
void* incr_thread(void* param)
{
    int i;
    for(i=1;i<=30;i++)
    {
        printf("incr(%d)  : %d.\n",gettid,i);
        sleep(1);
    }
    return NULL;
}
void* decr_thread(void* param)
{
    int i;
    for(i=30;i>0;i--)
    {
        printf("decr(%d)  : %d.\n",gettid,i);
        sleep(1);
    }
    return NULL;
}
int main()
{
    int i,ret;
    pthread_t it,dt;
    ret = pthread_create(&it,NULL,incr_thread,NULL);
    ret = pthread_create(&dt,NULL,decr_thread,NULL);
    for(i=0;i<30;i++)
    {
        printf("Count(%d)    :   %d\n",getpid(),i);
        sleep(1);
    }
    return 0;
}