#include<stdio.h>

#include<unistd.h>
#include<sys/wait.h>
#include<sys/shm.h>


#define SHM_KEY 0x1234
#define SEM_KEY 0x4321

union semun {
	int val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO (Linux-specific) */
};

typedef struct shm{
	int cnt;
}shm_t;

int main()
{
	int ret,i,s;
	shm_t *ptr;
	int shmid;
	shmid = shmget(SHM_KEY,sizeof(shm_t),IPC_CREAT | 0600);
		if(shmid<0)
		{
			perror("shmget() failed.\n");
			_exit(1);
		}
		printf("shmget() is done\n");
		getchar();
	 ptr =(shm_t *)shmat(shmid,NULL,0);
	if(ptr==(void *)-1)
	{
		perror("shmat() failed.\n");
		shmctl(shmid,IPC_RMID,NULL);
		_exit(1);
	}
	printf("shmat() is done");
	getchar();
	ptr->cnt=0;

	ret = fork();
	if(ret == 0)
	{
		for(i=1;i<=10;i++)
		{
			ptr->cnt--;
			printf("child	:	(pid	:	%d) %d\n",getpid(),ptr->cnt);
			sleep(1);
		}
		shmdt(ptr);
	}
	else
	{
		for(i=1;i<=10;i++)
		{
			ptr->cnt++;
			printf("parent	:	(pid	:	%d) %d\n",getpid(),ptr->cnt);
			sleep(1);
		}
		waitpid(-1,&s,0);
		printf("final count	%d\n",ptr->cnt);
		getchar();

		printf("shmdt() done\n");
		shmdt(ptr);
		
		getchar();
	}
	shmctl(shmid,IPC_RMID,NULL);
	printf("shmctl() is done");
	getchar();
	return 0;
}
