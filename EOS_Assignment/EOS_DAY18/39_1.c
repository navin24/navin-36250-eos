#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>
#include<sys/sem.h>
#include<sys/shm.h>
#define SHM_KEY 0x1234
#define SEM_KEY 0x4321

union semun {
	int val;    /* Value for SETVAL */
	struct semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
	unsigned short  *array;  /* Array for GETALL, SETALL */
	struct seminfo  *__buf;  /* Buffer for IPC_INFO (Linux-specific) */
};

typedef struct shm{
	int cnt;
}shm_t;

int main()
{
	int ret,i,s;
	shm_t *ptr;
	int shmid,semid;
	struct sembuf ops[1];
	union semun su;
	semid = semget(SEM_KEY,1,IPC_CREAT | 0600);
	if(semid<0)
	{
		perror("semget() failed.\n");
		_exit(1);
	}
	su.val = 1;
	ret = semctl(semid,0,SETVAL,su);
	if(ret<0)
	{
		perror("semctl() failed\n");
		_exit(1);
	}
	shmid = shmget(SHM_KEY,sizeof(shm_t),IPC_CREAT | 0600);
		if(shmid<0)
		{
			perror("shmget() failed.\n");
			_exit(1);
		}
	 ptr =(shm_t *)shmat(shmid,NULL,0);
	if(ptr==(void *)-1)
	{
		perror("shmat() failed.\n");
		shmctl(shmid,IPC_RMID,NULL);
		_exit(1);
	}
	ptr->cnt=0;

	ret = fork();
	if(ret == 0)
	{
		for(i=1;i<=10;i++)
		{
			ops[0].sem_num = 0;
			ops[0].sem_op = -1;
			ops[0].sem_flg = 0;
			semop(semid,ops,1);
			ptr->cnt--;
			printf("child	:	(pid	:	%d) %d\n",getpid(),ptr->cnt);
			sleep(1);
			ops[0].sem_num = 0;
			ops[0].sem_op = +1;
			ops[0].sem_flg = 0;
			semop(semid,ops,1);
		}
		shmdt(ptr);
	}
	else
	{
		for(i=1;i<=10;i++)
		{
			ops[0].sem_num = 0;
			ops[0].sem_op = -1;
			ops[0].sem_flg = 0;
			semop(semid,ops,1);
			ptr->cnt++;
			printf("parent	:	(pid	:	%d) %d\n",getpid(),ptr->cnt);
			sleep(1);
			ops[0].sem_num = 0;
			ops[0].sem_op = +1;
			ops[0].sem_flg = 0;
			semop(semid,ops,1);
		}
		waitpid(-1,&s,0);
		printf("final count	%d\n",ptr->cnt);
		shmdt(ptr);
		shmctl(shmid,IPC_RMID,NULL);
		semctl(semid,0,IPC_RMID);
	}
	
	return 0;
}
