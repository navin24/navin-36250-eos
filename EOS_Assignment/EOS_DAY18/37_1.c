#include<stdio.h>

#include<unistd.h>
#include<sys/wait.h>
#include<sys/shm.h>


#define SHM_KEY 0x1234

typedef struct shm{
	int cnt;
}shm_t;

int main()
{
	int ret,i,s;
	shm_t *ptr;
	int shmid;
	shmid = shmget(SHM_KEY,sizeof(shm_t),IPC_CREAT | 0600);
		if(shmid<0)
		{
			perror("shmget() failed.\n");
			_exit(1);
		}
	 ptr =(shm_t *)shmat(shmid,NULL,0);
	if(ptr==(void *)-1)
	{
		perror("shmat() failed.\n");
		shmctl(shmid,IPC_RMID,NULL);
		_exit(1);
	}
	ptr->cnt=0;

	ret = fork();
	if(ret == 0)
	{
		for(i=1;i<=30;i++)
		{
			ptr->cnt--;
			printf("child	:	(pid	:	%d) %d\n",getpid(),ptr->cnt);
			sleep(1);
		}
		shmdt(ptr);
	}
	else
	{
		for(i=1;i<=30;i++)
		{
			ptr->cnt++;
			printf("parent	:	(pid	:	%d) %d\n",getpid(),ptr->cnt);
			sleep(1);
		}
		waitpid(-1,&s,0);
		printf("final count	%d\n",ptr->cnt);
		shmdt(ptr);
	}
	shmctl(shmid,IPC_RMID,NULL);
	return 0;
}
