#!/bin/bash

echo -n "Enter a path:"
read path

if [ ! -e $path ]
then
    echo "path doesn't exists."
    exit
fi

if [ -d $path ]
then
	cd $path
	ls
fi

if [ -f $path ] 
then
	res=`stat -c %U $path`
	echo "Owner of file is:$res"
fi
