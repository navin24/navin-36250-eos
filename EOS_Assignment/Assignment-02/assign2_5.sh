#!/bin/bash

echo -n "Enter a filename:"
read file

if [ ! -e $file ]
then
     echo "File doesn't exists."
     exit
else
	res=`stat -c %y $file`
	echo "Last Modified Time: $res"
fi

