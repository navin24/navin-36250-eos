#!/bin/bash

echo -n "Enter a number:"
read num

i=1
while [ $i -le 10 ]
do
	res=`expr $num \* $i`
	echo "$num * $i = $res"
	i=`expr $i + 1`
#	i=`expr $i+1` Operand and operator need space
done
