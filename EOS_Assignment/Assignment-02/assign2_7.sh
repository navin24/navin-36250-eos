#!/bin/bash

echo -e "\n1. Date"
echo -e "\n2. Calendar"
echo -e "\n3. List of files"
echo -e "\n4. Current Directory"
echo -e "\n5. Exit"

echo -n "Which option you want to choose(from above list):"
read num

case $num in
1)
	echo -n "Today's Date is:"
	date
	;;
2)
	echo "Calendar of present year:"
	cal
	;;
3)
	echo "List of files of current directory"
	ls
	;;
4)
	echo -n "Current Directory:" 
	pwd
	;;
5)
	echo "Bye"
	exit
	;;
*)
	echo "Unknown"
esac


