#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>

int main() {
    int arr[2], ret, s;
    char buf1[512], buf2[512];

    ret = pipe(arr);
    if(ret < 0) {
        perror("pipe() failed");
        _exit(1);
    }

    ret = fork();

    if(ret == 0) {
        close(arr[0]); // close read end of pipe in child    

        printf("child: enter a string: \n");
        scanf("%s", buf1);

        printf("child: written into pipe: %s\n", buf1);
        write(arr[1], buf1, strlen(buf1)+1); // write into pipe from child

        close(arr[1]); // close write end of pipe in child
        _exit(0);
    }
    else {
        close(arr[1]); // close write end of pipe in parent

        printf("parent: waiting for the data.\n");

        read(arr[0], buf2, sizeof(buf2)); // read from pipe into parent
        printf("parent: read from pipe: %s\n", buf2);
        
        close(arr[0]); // close read end of pipe in parent
 
        waitpid(-1, &s, 0);
    }
    return 0;
}
