#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

int main() {
    int ret, err, s;
    printf("parent started.\n");
    ret = fork();
    if(ret == 0) {
        int fd_out = open("out.txt", O_CREAT | O_WRONLY | O_TRUNC, 0644);
        close(1);
        dup(fd_out);
        close(fd_out);

        err = execlp("ls", "ls", "-l", NULL);
        if(err < 0) {
            perror("exec() failed");
            _exit(1);
        }
    }
    else {
        waitpid(-1, &s, 0);
        printf("child exit: %d\n", WEXITSTATUS(s));
    }
    printf("parent completed.\n");
    return 0;
}