#include<unistd.h>
#include<string.h>
#include<stdio.h>

int main()
{
    int arr[2], ret;
    ret = pipe(arr);
     char buf1[512], buf2[512];
    if(ret<0)
    {
        perror("pipe() failed");
        _exit(1);
    }
    printf("Enter a string  :   ");
    scanf("%s",buf1);
    write(arr[1],buf1,strlen(buf1)+1);
    printf("data written    :   %s",buf1);

        read(arr[0],buf2,sizeof(buf2));
    printf("data from pipe    :   %s",buf2);

    close(arr[1]);
    close(arr[0]);


}

