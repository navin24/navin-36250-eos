#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <fcntl.h>

int main() {
    int ret, err, s;
    printf("parent started.\n");
    ret = fork();
    if(ret == 0) {
        int fd_in, fd_out;
        /*
        // input rediection        
        fd_in = open("day20.md", O_RDONLY);
        close(STDIN_FILENO);
        dup(fd_in);
        close(fd_in);

        // output redirection
        fd_out = open("out.txt", O_CREAT | O_WRONLY | O_TRUNC, 0644);
        close(STDOUT_FILENO);
        dup(fd_out);
        close(fd_out);
        */

        // input rediection        
        fd_in = open("day17.md", O_RDONLY);
        dup2(fd_in, STDIN_FILENO);
        close(fd_in);

        // output redirection
        fd_out = open("out.txt", O_CREAT | O_WRONLY | O_TRUNC, 0644);
        dup2(fd_out, STDOUT_FILENO);
        close(fd_out);

        err = execlp("wc", "wc", NULL);
        if(err < 0) {
            perror("exec() failed");
            _exit(1);
        }
    }
    else {
        waitpid(-1, &s, 0);
        printf("child exit: %d\n", WEXITSTATUS(s));
    }
    printf("parent completed.\n");
    return 0;
}