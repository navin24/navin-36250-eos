#include <stdio.h>
#include <unistd.h>
#include <sys/msg.h>
#include <sys/wait.h>

#define MQ_KEY  0x2405
#define MSG_SZ (sizeof(msg_t) - sizeof(long))

typedef struct msg {
    long type; // header
    int data; // body
}msg_t;

int main() {
    int ret, s, mqid;
    
    mqid = msgget(MQ_KEY, IPC_CREAT | 0600);
    // ...

    ret = fork();
    if(ret == 0) {
        // child process - sender
        msg_t m1;
        printf("child: enter a string: \n");
        scanf("%d", &m1.data);
        m1.type = 123;

        printf("child: sending message: %d\n", m1.data);
        ret = msgsnd(mqid, &m1, MSG_SZ, 0);        
        // ...
        _exit(0);
    } else {
        // parent process - receiver
        msg_t m2;

        printf("parent: waiting for the messge.\n");
        ret = msgrcv(mqid, &m2, MSG_SZ, 123, 0);
        // ...

        printf("parent: received message: %d\n", m2.data);

        waitpid(-1, &s, 0);

        msgctl(mqid, IPC_RMID, NULL);
    }
    return 0;
}