#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(int argc, char *argv[])
{
	int ret;
	if(argc!=2)
	{
		fprintf(stderr,"Syntax: %s <file path>\n",argv[0]);
			exit(1);
	}
	ret=unlink(argv[1]);
	if(ret<0)
	{
		printf("Unlink() Failed\n");
		exit(1);
	}

return 0;
}
