#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(int argc, char *argv[])
{
	int ret;
	if(argc!=3)
	{
		fprintf(stderr,"Syntax: %s <target file path> <link path>\n",argv[0]);
			exit(1);
	}
	ret=link(argv[1],argv[2]);
	if(ret<0)
	{
		printf("Link() Failed\n");
		exit(1);
	}

return 0;
}
