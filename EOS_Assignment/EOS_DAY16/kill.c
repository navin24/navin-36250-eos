#include<stdio.h>
#include<unistd.h>

int main(int argc , char* argv[])
{
	if(argc!=3)
	{
		perror("Not appropriate arguments\n");
	}
	int ret,sig,pid;
	sig = atoi(argv[1]);
	pid = atoi(argv[2]);
	ret=kill(pid,sig);
	if(ret ==0)
		printf("Kill system call succesfully executed\n");	
	else
		printf("Please provide appropriate signal number or process ID\n");
	return 0;
}


