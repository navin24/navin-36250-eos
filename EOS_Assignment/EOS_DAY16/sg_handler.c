#include<stdio.h>
#include<unistd.h>
#include<signal.h>
void sg_handler_int(int sig)
{
	printf("i am inside this signal(%d) handler\n",sig);
}
void sg_handler_term(int sig)
{
	printf("i am inside this signal(%d) handler\n",sig);
}
//void sg_handler_kill(int sig)
//{
//	printf("i am inside this signal(%d) handler\n",sig);
//}
int main()
{
	signal(2,sg_handler_int);
	signal(15,sg_handler_int);
	signal(9,sg_handler_kill);
	int i=1;
	while(1)
	{
		printf("Running (%d): %d\n",getpid(),i);
		i++;
		sleep(1);
	}
	return 0;
}
