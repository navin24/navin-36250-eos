#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

void sig_handler(int sig) {
    switch (sig)
    {
    case SIGINT:
        printf("signal SIGINT (%d) handled.\n", sig);
        break;
    case SIGTERM:
        printf("signal SIGTERM (%d) handled.\n", sig);
        break;
    case SIGSEGV:
        printf("signal SIGSEGV (%d) handled.\n", sig);
        _exit(1);
        break;    
    }
}

int main() {
    struct sigaction sa;
    int ret, i;
    
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sig_handler;
    ret = sigaction(SIGINT, &sa, NULL);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sig_handler;
    ret = sigaction(SIGTERM, &sa, NULL);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sig_handler;
    ret = sigaction(SIGSEGV, &sa, NULL);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }

    i=1;
    while(1) {
        printf("running (%d) : %d\n", getpid(), i++);
        sleep(1);
    }
    return 0;
}