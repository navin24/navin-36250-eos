#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

void sigint_handler(int sig) {
    printf("sigint(%d) handled\n",sig);
}

int main() {
    int i, ret, s;
    struct sigaction sa;

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT, &sa, NULL);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }

	printf("process is blocked\n");
	printf("enter a handler\n");
	pause();
	printf("TataBye\n");

    return 0;
}
