#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

void sigint_handler(int sig) {
    printf("sigint(%d) handled\n",sig);
}

int main() {
    int i, ret, s;
    struct sigaction sa;
	sigset_t s1,s2;
     sigfillset(&s1);
     sigemptyset(&s2);
	 sigdelset(&s1,SIGINT);
     ret=sigprocmask(SIG_SETMASK,&s1,&s2);
     if(ret != 0) {
		  perror("sigprocmask() failed");
		  _exit(1);
     }

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigint_handler;
    ret = sigaction(SIGINT, &sa, NULL);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }

	int cnt=1;
	while(1)
	{
		printf("Process	:	(id	:	%d) %d\n", getpid(),cnt);
		++cnt;
        sleep(1);

	}
    return 0;
}
