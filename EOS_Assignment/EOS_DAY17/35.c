#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

void sigint_handler(int sig) {
	int i ;
	for(i=0;i<10;i++)
	{
    	printf("sigint(%d) handled\n",sig);
		sleep(1);
	}
}

int main() {
    int i, ret, s;
    struct sigaction sa;

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigint_handler;
	sigfillset(&sa.sa_mask);
    ret = sigaction(SIGINT, &sa, NULL);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }
	i=1;
	while(1)
	{
		printf("Count	:	%d\n",i);
		i++;
		sleep(1);
	}
    return 0;
}
