#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

void sigint_handler(int sig) {
    printf("signal (%d) handled.\n", sig);
}
void sigterm_handler(int sig) {
    printf("signal (%d) handled.\n", sig);
}

int main() {
    struct sigaction sa1, sa2;
    int ret, i;
    memset(&sa1, 0, sizeof(sa1));
    memset(&sa2, 0, sizeof(sa2));   
    sa1.sa_handler = sigint_handler;
    ret = sigaction(SIGINT, &sa1, &sa2);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }
    sa1.sa_handler = sigterm_handler;
    ret = sigaction(SIGTERM, &sa1, &sa2);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }

    i=1;
    while(1) {
        printf("running (%d) : %d\n", getpid(), i++);
        sleep(1);
    }
    return 0;
}
