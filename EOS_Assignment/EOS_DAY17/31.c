#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <signal.h>

void sigchld_handler(int sig) {
    // when child is terminated, SIGCHLD handler is called.
    int s;
    waitpid(-1, &s, 0);
    printf("child exit status: %d\n", WEXITSTATUS(s));
}

int main() {
    int i, ret, s;
    struct sigaction sa;

    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = sigchld_handler;
    ret = sigaction(SIGCHLD, &sa, NULL);
    if(ret != 0) {
        perror("sigaction() failed");
        _exit(1);
    }

    ret = fork();
    if(ret == 0) {
        // child process
        for(i=1; i<=20; i++) {
            printf("child : %d\n", i);
            sleep(1);
        }
        _exit(6);
		//When child is terminated it sends SIGCHLD signal to parent,
		//parent executes that particular handler and it gets child's 
		//exit status
    } else {
        // parent process
        for(i=1; i<=30; i++) {
            printf("parent : %d\n", i);
            sleep(1);
        }
    }
    return 0;
}
